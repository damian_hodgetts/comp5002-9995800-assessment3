﻿using System;

namespace menu
{
    class Program
    {
        static void Main(string[] args)
        {
            //Start the program with Clear();
            Console.Clear();
            
            var selection = 0;

            do {
                Console.Clear();
                Console.WriteLine("************************************");
                Console.WriteLine("*****          Menu            *****");
                Console.WriteLine("************************************");
                Console.WriteLine("*****                          *****");
                Console.WriteLine("*****        Option 1:         *****");
                Console.WriteLine("*****        Option 2:         *****");
                Console.WriteLine("*****        Option 3:         *****");
                Console.WriteLine("*****        Option 4:         *****");
                Console.WriteLine("*****                          *****");
                Console.WriteLine("************************************");
                Console.WriteLine();
                Console.WriteLine("Choose a number:");
                var t = Console.ReadLine();
                bool checkInput = int.TryParse(t, out selection);
                
                if (checkInput)
                {
                    switch (selection)
                    {
                        case 5:
                            Console.Clear();
                            Console.WriteLine("Thanks for using the program");
                        break;
                        default:
                            Console.WriteLine($"You selected option {selection}");
                            Console.WriteLine("Press <Enter> to quit the program"); 
                            Console.ReadKey();
                        break;
                    }
                }
                else
                {
                    Console.WriteLine("You didn't type in a number");
                    selection = 0;
                    Console.WriteLine("Press <Enter> to quit the program");
                    Console.ReadKey();
                }

                Console.WriteLine();

            }while(selection < 5);

            //End the program with blank line and instructions
            Console.ResetColor();
            Console.WriteLine();
            Console.WriteLine("Press <Enter> to quit the program");
            Console.ReadKey();
        }
    }
}
